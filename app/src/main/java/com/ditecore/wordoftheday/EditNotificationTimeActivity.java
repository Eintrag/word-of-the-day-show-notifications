package com.ditecore.wordoftheday;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditNotificationTimeActivity extends AppCompatActivity {
    private TextView tvDate;
    Calendar myCalendar = Calendar.getInstance();
    private long interval = 1000 * 60 * 60;
    private String unit = "HOURS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_notification_time);
        tvDate = findViewById(R.id.tvDate);

        initIntervalNumberPicker();
        initUnitsSpinner();

        myCalendar = Calendar.getInstance();
        updateLabel(myCalendar.getTime());
    }

    private void initIntervalNumberPicker() {
        NumberPicker np = findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(72);
        np.setOnValueChangedListener(onValueChangeListener);
    }

    private void initUnitsSpinner() {
        Spinner spinner = findViewById(R.id.spUnits);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.units, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    NumberPicker.OnValueChangeListener onValueChangeListener =
            new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                    interval = numberPicker.getValue() * 1000; //seconds
                    if (unit.equals("HOURS")) {
                        interval = interval * 60 * 60;
                    }
                    if (unit.equals("DAYS")) {
                        interval = interval * 60 * 60 * 24;
                    }
                }
            };

    TimePickerDialog.OnTimeSetListener date = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hour, int minute) {
            myCalendar.set(Calendar.HOUR, hour);
            myCalendar.set(Calendar.MINUTE, minute);
            updateLabel(myCalendar.getTime());
        }
    };

    public void setDate(View view) {
        new TimePickerDialog(
                EditNotificationTimeActivity.this, date,
                myCalendar.get(Calendar.HOUR),
                myCalendar.get(Calendar.MINUTE),
                true
        ).show();
    }

    private void updateLabel(Date date) {
        String myFormat = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        tvDate.setText(sdf.format(date));
    }

    public void scheduleNotification(View view) {
        new NotificationScheduler().scheduleRandom(this, myCalendar.getTimeInMillis(), interval);
    }

}