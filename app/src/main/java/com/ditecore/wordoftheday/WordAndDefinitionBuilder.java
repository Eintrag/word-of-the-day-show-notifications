package com.ditecore.wordoftheday;

import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class WordAndDefinitionBuilder {

    private ArrayList<WordAndDefinitionList.WordAndDefinition> wordAndDefinitionList;

    public WordAndDefinitionBuilder(Context context) {
        String myJson = inputStreamToString(context.getResources().openRawResource(R.raw.german_to_english));
        wordAndDefinitionList = new Gson().fromJson(myJson, WordAndDefinitionList.class).list;
    }

    private String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            return new String(bytes);
        } catch (IOException e) {
            return null;
        }
    }

    public WordAndDefinitionList.WordAndDefinition getRandomEntry() {
        int randomNum = new Random().nextInt(wordAndDefinitionList.size());
        return wordAndDefinitionList.get(randomNum);
    }
}
