package com.ditecore.wordoftheday;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class SwipingActivity extends AppCompatActivity {
    private ArrayList<WordAndDefinitionList.WordAndDefinition> shownWords = new ArrayList<>();
    private int index = 0;
    private WordAndDefinitionBuilder wordAndDefinitionBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        wordAndDefinitionBuilder = new WordAndDefinitionBuilder(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swiping);
    }

    private void showNextWord() {
        // TODO activate on swipe right
        WordAndDefinitionList.WordAndDefinition nextWord = wordAndDefinitionBuilder.getRandomEntry();
        shownWords.add(nextWord);
        index += 1;
        showWord(nextWord);
    }

    private void showPreviousWord() {
        // TODO activate on swipe left
        if(shownWords.size() > 0 && index > 0) {
            WordAndDefinitionList.WordAndDefinition nextWord;
            index -= 1;
            try {
                nextWord = shownWords.get(index);
            } catch (IndexOutOfBoundsException e) {
                index = 0;
                nextWord = shownWords.get(index);
            }
            showWord(nextWord);
        }
    }

    private void showWord(WordAndDefinitionList.WordAndDefinition word) {

    }
}
