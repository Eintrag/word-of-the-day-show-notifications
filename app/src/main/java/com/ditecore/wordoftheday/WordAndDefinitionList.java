package com.ditecore.wordoftheday;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WordAndDefinitionList {
    @SerializedName("list")
    public ArrayList<WordAndDefinition> list;

    static public class WordAndDefinition {
        @SerializedName("english")
        private String english;
        @SerializedName("german")
        private String german;

        public String getEnglish() {
            return english;
        }
        public String getGerman() {
            return german;
        }
    }
}
