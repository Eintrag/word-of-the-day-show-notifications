package com.ditecore.wordoftheday;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

public class NotificationScheduler {

    public void scheduleRandom(Context context, long triggerAtMillis, long interval) {
        WordAndDefinitionList.WordAndDefinition randomEntry = new WordAndDefinitionBuilder(context).getRandomEntry();
        schedule(context, triggerAtMillis, randomEntry.getGerman(), randomEntry.getEnglish(), interval);
    }

    public void schedule(Context context, long triggerAtMillis, String title, String description, long interval) {

        Intent MyIntent = new Intent(context, NotificationPublisher.class);
        MyIntent.putExtra("german", title);
        MyIntent.putExtra("english", description);
        MyIntent.putExtra("notifId", Calendar.getInstance().getTimeInMillis());
        MyIntent.putExtra("interval", interval);

        PendingIntent MyPendIntent = PendingIntent.getBroadcast(context, 0, MyIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager MyAlarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        MyAlarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, MyPendIntent);
    }

}
