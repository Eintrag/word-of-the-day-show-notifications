package com.ditecore.wordoftheday;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.ditecore.wordoftheday.MainActivity.NOTIFICATION_CHANNEL_ID;

public class NotificationPublisher extends BroadcastReceiver {

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd MMM HH:mm");

    public void onReceive(Context context, Intent intent) {

        Intent resultIntent = new Intent(context, EditNotificationTimeActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(intent.getStringExtra("german"))
                .setContentText(intent.getStringExtra("english"))
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        // notificationId is a unique int for each notification that you must define
        int notificationId = (int) intent.getLongExtra("notifId", 0);
        long interval = intent.getLongExtra("interval", 0);
        notificationManager.notify(notificationId, builder.build());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Calendar.getInstance().getTimeInMillis() + interval);
        showToast(context, cal);
        new NotificationScheduler().scheduleRandom(context, cal.getTimeInMillis(), interval);
    }

    private void showToast(Context context, Calendar cal) {
        Toast.makeText(context, "Next word at " + SIMPLE_DATE_FORMAT.format(cal.getTime()), Toast.LENGTH_SHORT).show();
    }
}